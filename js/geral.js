$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL DE DESTAQUE
	$("#carrossel-destaque").owlCarousel({
    items : 1,
    dots: true,
    loop: false,
    lazyLoad: true,
    mouseDrag:true,
    touchDrag  : true,	       
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});


	$(window).bind('scroll', function () {
     
     var alturaScroll = $(window).scrollTop();

       if (alturaScroll >= 200) {

          $(".topo").addClass("topoativo");

       } else {

            $(".topo").removeClass("topoativo");

       }
   });

	$('.indice ul li a').click(function(e) {
       
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }

    });

    // Função de clique
    // "(e)" ou "(event)": selciona apenas o que for clicado
    $('.pg-sobre .area-titulo .indice ul li a').click(function(e) {

        // Remove a classe
        $( '.pg-sobre .area-titulo .indice ul li a' ).removeClass( "ativo" );

        // Função que adiciona a classe
        // this: Adiciona a classe apenas no que for clicado
       $( this ).addClass( "ativo" );
    });




    $('.pg-servicos .servicos .processo-cobranca nav span').click(function(e) {
      
      let dataID = $(this).attr("data-id");

      $(".areaDescricao #"+dataID).show();
      
    });

    
  
});